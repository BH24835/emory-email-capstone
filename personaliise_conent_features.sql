
Create table mcy-eda-anlytcs-sbox.emory_personalize_content.campaign_data as
 SELECT *,
case
when ((LOWER(treatment_code) LIKE '%cc%' OR LOWER(treatment_code) LIKE '%gss%' OR LOWER(treatment_code) LIKE '%cfobhb%') AND (LOWER(Treatment_Code) NOT LIKE '%rtwcc%' AND LOWER(Treatment_Code) NOT LIKE '%cfobcc%' AND LOWER(Treatment_Code) NOT LIKE '%cfobacc%' AND LOWER(Treatment_Code) NOT LIKE '%cfobocc%')) then 'FOB_CC'
when (LOWER(Treatment_Code) LIKE '%cfob%' OR LOWER(Treatment_Code) LIKE '%apparelessential%') AND (LOWER(Treatment_Code) NOT LIKE '%cfobhb%')   then 'FOB_CFOB'
when ((LOWER(Treatment_Code)  LIKE '%home%' or LOWER(Treatment_Code)  LIKE '%hme%' or LOWER(Treatment_Code)  LIKE '%travelluggageods%') AND (LOWER(Treatment_Code) NOT LIKE '%m_rtw%' AND LOWER(Treatment_Code) NOT LIKE '%cfob%')) then 'FOB_HOME'
when (LOWER(Treatment_Code) LIKE '%kid%') AND (LOWER(Treatment_Code) NOT LIKE '%cfob%') then 'KIDS_TABLE_FOB'
when ((LOWER(Treatment_Code) LIKE '%mens%' OR LOWER(Treatment_Code) LIKE '%mns%') AND (LOWER(Treatment_Code) NOT LIKE '%cfob%' AND LOWER(Treatment_Code) NOT LIKE '%cc%' AND LOWER(Treatment_Code) NOT LIKE '%cmn%')) then 'FOB_MENS'
when (LOWER(Treatment_Code) LIKE '%leas%') then 'FOB_LEASED'
when (( LOWER(Treatment_Code) LIKE '%_crd%'OR LOWER(Treatment_Code) LIKE '% _lty%'OR LOWER(Treatment_Code) LIKE '%giftcard%'OR LOWER(Treatment_Code) LIKE '%noproeagl%') AND LOWER(Treatment_Code) NOT LIKE '%_ltyenhapp%'  
AND LOWER(Treatment_Code) NOT LIKE '%_site%') then 'FOB_LOYALTY'
when ((LOWER(Treatment_Code)  LIKE '%m_rtw%'or LOWER(Treatment_Code)  LIKE '%_ttlrtw%' or LOWER(Treatment_Code) like '%pbrtw%') AND (LOWER(Treatment_Code) not like'%m_rtwcc%')) then 'FOB_RTW'
when (LOWER(Treatment_Code) like '%m_rtwcc%') then 'FOB_RTWCC'
when ((LOWER(Treatment_Code) like '%beauty%' or LOWER(Treatment_Code) like'%bty%')) then 'FOB_BEAUTY'
when (LOWER(Treatment_Code) like '%site%' OR LOWER(Treatment_Code) LIKE '%m_shq%') then 'FOB_SITE'
else 'FOB_OTHER' end as FOB
 FROM `mcy-eda-anlytcs-sbox.email_frequency.email_data_by_camp_TEMP_1` ;

create table mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB as 
(select indiv_id,FOB, SUM(tot_sent_email_day_count_by_campaign) AS sent_one_per_day -- count days the email sent (single)
, SUM(IF(bounce_by_campaign>0,1,0)) AS bounced_single -- count bounced email one per campaign (single)
, SUM(IF(delivered_by_campaign>0,1,0)) AS delivered_single -- count delivered email one per campaign (single)
, SUM(IF(open_by_campaign>0,1,0)) AS opened_single -- count opened email one per campaign (single)
, SUM(IF(click_by_campaign>0,1,0)) AS clicked_single -- count clicked email one per campaign (single)
,safe_divide(SUM(IF(click_by_campaign>0,1,0)), SUM(IF(delivered_by_campaign>0,1,0))) as ctd
,safe_divide(SUM(order_by_campaign), SUM(IF(delivered_by_campaign>0,1,0))) as otd
from mcy-eda-anlytcs-sbox.emory_personalize_content.campaign_data
group by indiv_id,FOB);


create table mcy-eda-anlytcs-sbox.emory_personalize_content.email_prioritisation_features as
select  a.*
,c.ctd as cfob_ctd, c.otd as    cfob_otd
,d.ctd as cc_ctd, d.otd as      cc_otd
,e.ctd as kids_ctd, e.otd as    kids_otd
,f.ctd as mens_ctd, f.otd as    mens_otd
,g.ctd as leased_ctd, g.otd as  leased_otd
,h.ctd as loyalty_ctd, h.otd as loyalty_otd
,i.ctd as home_ctd, i.otd as    home_otd
,j.ctd as rtw_ctd, j.otd as     rtw_otd
,k.ctd as rtwcc_ctd, k.otd as   rtwcc_otd
,l.ctd as beauty_ctd, l.otd as  beauty_otd
,m.ctd as site_ctd, m.otd as    site_otd
,n.ctd as other_ctd, n.otd as   other_otd

 from `mcy-eda-anlytcs-sbox.email_frequency.contact_frequency_features_v1_01_10_2024`
 a
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_CFOB')
 c 
on a.indiv_id=c.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_CC')
 d 
on a.indiv_id=d.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='KIDS_TABLE_FOB')
 e 
on a.indiv_id=e.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_MENS')
 f 
on a.indiv_id=f.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_LEASED')
 g 
on a.indiv_id=g.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_LOYALTY')
 h 
on a.indiv_id=h.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_Home')
 i 
on a.indiv_id=i.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_RTW')
 j 
on a.indiv_id=j.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_RTWCC')
 k
on a.indiv_id=k.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_BEAUTY')
 l 
on a.indiv_id=l.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_SITE')
 m 
on a.indiv_id=m.indiv_id
left join 
(select indiv_id,ctd,otd from mcy-eda-anlytcs-sbox.emory_personalize_content.individ_FOB
where FOB='FOB_OTHER')
 n 
on a.indiv_id=n.indiv_id;
